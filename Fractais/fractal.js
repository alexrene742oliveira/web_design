angle = 45; //Define o angulo dos ramos

function setup() {
    createCanvas(600, 500); //Cria o canvas
    stroke(255); //Pinta as linhas que serão a arvore de branco
    angleMode(DEGREES); //Mudar o angulo de radianos para graus
    
    sldAng = createSlider(0,360,45); //Cria um slider para o angulo dos galhos
    sldSize = createSlider(0,1,0.66,0); //Slider para o tamanho dos galhos
}

function draw() {
    background(51); //Cria um fundo escuro
    translate(width/2, height); //Move do ponto (0,0) ate o meio do canvas
    
    angle = sldAng.value(); //O angulo dos galhos é ditado pelo slider esquerdo
    
    drawFractal(150); //Desenha a arvore de fractais
}

function drawFractal(len)
{
    if(len < 5) //Condição para interromper o loop de fractais
    {
        fill(255,0,0); //Preenche os circulos de vermelho
        circle(0,0,5); //Cria pequenos circulos para simular maças
        return;
    }

    line(0,0,0,-len); //Desenha o tronco da arvore no meio do canvas
    translate(0,-len); //Move o cursor do desenho para cima
    
    push(); //Salva o estado do desenho
    rotate(angle); //Rotaciona os galhos para a direita
    drawFractal(len*sldSize.value()); //Desenha o fractal
    pop(); // Retorna o estado original do desenho

    // Mesmo procedimento para o lado esquerdo
    push();
    rotate(-angle);
    drawFractal(len*sldSize.value());
    pop();
}